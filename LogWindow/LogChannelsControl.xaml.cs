﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace LoggingWindow
{
    public partial class LogChannelsControl : UserControl
    {
        public LogWindowControl MyLogWindowControl { get; set; }
        public LogChannels MyLogChannelsWindow { get; set; }

        public LogChannelsControl()
        {
            InitializeComponent();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            MyLogWindowControl.RefreshLog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var channel = (Channel)(sender as Button).DataContext;
            System.Windows.Forms.ColorDialog d = new System.Windows.Forms.ColorDialog();
            d.Color = channel.Color.ToDrawingColor();
            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var c = d.Color;
                MyLogWindowControl.ChangeColor(channel.Text, Color.FromRgb(c.R, c.G, c.B));
            }
        }

        private void Up_Click(object sender, RoutedEventArgs e)
        {
            int index = Config.SelectedIndex;
            if (index > 0)
            {
                var l = MyLogWindowControl;
                var t = LogWindowControl.log_channels[index - 1];
                LogWindowControl.log_channels[index - 1] = LogWindowControl.log_channels[index];
                LogWindowControl.log_channels[index] = t;
                l.RefreshLog();
                Config.SelectedIndex = index - 1;
            }
        }

        private void Down_Click(object sender, RoutedEventArgs e)
        {
            int index = Config.SelectedIndex;
            if (index != -1 && index < Config.Items.Count - 1)
            {
                var l = MyLogWindowControl;
                var t = LogWindowControl.log_channels[index + 1];
                LogWindowControl.log_channels[index + 1] = LogWindowControl.log_channels[index];
                LogWindowControl.log_channels[index] = t;
                l.RefreshLog();
                Config.SelectedIndex = index + 1;
            }
        }

        private void remove_button_click(object sender, RoutedEventArgs e)
        {
            var channel = (sender as FrameworkElement).DataContext as Channel;
            var l = MyLogWindowControl;
            int id = LogWindowControl.log_channels.IndexOf(channel);
            if (id != -1)
            {
                LogWindowControl.log_channels.RemoveAt(id);
                l.RefreshLog();
            }
        }

        public static Visibility boolToVisible(bool b)
        {
            return b ? Visibility.Visible : Visibility.Collapsed;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var lvi = sender as TextBox;
            var channel = lvi.DataContext as Channel;
            channel.Text = lvi.Text;
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                MyLogWindowControl.RefreshLog();
            }
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            LogWindowControl.log_channels.Insert(0, new Channel { Text = "regex", Enabled = true, Color = LogWindowPackage.default_channel_color });
        }
    }
}
