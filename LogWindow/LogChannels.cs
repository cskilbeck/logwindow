﻿namespace LoggingWindow
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio.Shell;

    [Guid("75ce3f85-bad9-4a1f-b19a-878d53881a05")]
    public class LogChannels : ToolWindowPane
    {
        LogWindowControl log_window_control;

        public LogWindowControl LogWindowControl
        {
            get
            {
                return log_window_control;
            }
            set
            {
                log_window_control = value;
                (this.Content as LogChannelsControl).MyLogWindowControl = value;
            }
        }

        public LogChannels() : base(null)
        {
            this.Caption = "Log Channels";
            this.Content = new LogChannelsControl();
            (this.Content as LogChannelsControl).MyLogChannelsWindow = this;
        }
    }
}
