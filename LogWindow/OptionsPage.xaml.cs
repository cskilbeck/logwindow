﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoggingWindow
{
    public partial class OptionsPage : UserControl
    {
        private Options options;

        public OptionsPage(Options o)
        {
            InitializeComponent();
            options = o;
            DataContext = o;
        }

        public Options Options
        {
            get
            {
                return options;
            }
        }

        private void reset_button_Click(object sender, RoutedEventArgs e)
        {
            options.ResetSettings();
        }

        private void font_button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FontDialog f = new System.Windows.Forms.FontDialog();
            f.ShowColor = false;
            f.ShowEffects = false;
            f.ShowApply = false;
            f.Font = options.CurrentFont;
            if(f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                options.CurrentFont = f.Font;

            }
        }

        private void timestamp_format_textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (timestamp_format_textBlock == null) return;
            DateTime t = DateTime.Now;
            try
            {
                timestamp_format_textBlock.Text = t.ToString(timestamp_format_textBox.Text);
                options.TimestampFormat = timestamp_format_textBox.Text;

            }
            catch (FormatException)
            {
                timestamp_format_textBlock.Text = "Invalid";
            }
        }
    }
}
