﻿using System;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Debugger.Interop;
using System.Runtime.InteropServices;

namespace LoggingWindow
{
    public class DebuggerEvents : DisposableObject, IVsDebuggerEvents
    {
        bool started;
        private IVsDebugger debugger;
        private uint debugEventsCookie = VSConstants.VSCOOKIE_NIL;

        public delegate void DebuggerStartedDelegate(uint process_id);
        public delegate void DebuggerEndedDelegate();

        public event DebuggerStartedDelegate DebuggerStarted;
        public event DebuggerEndedDelegate DebuggerEnded;

        public virtual void OnDebuggerStarted(uint process_id)
        {
            DebuggerStarted?.Invoke(process_id);
        }

        public virtual void OnDebuggerEnded()
        {
            DebuggerEnded?.Invoke();
        }

        public DebuggerEvents(IVsDebugger debugger_instance)
        {
            debugger = debugger_instance;
            debugger.AdviseDebuggerEvents(this, out debugEventsCookie);
        }

        protected override void DisposeManagedResources()
        {
            ThreadHelper.Generic.Invoke(() =>
            {
                if (debugEventsCookie != VSConstants.VSCOOKIE_NIL && debugger != null)
                {
                    ErrorHandler.CallWithCOMConvention(() => debugger.UnadviseDebuggerEvents(debugEventsCookie));
                    debugEventsCookie = VSConstants.VSCOOKIE_NIL;
                }
            });
            base.DisposeManagedResources();
        }

        public int OnModeChange(DBGMODE dbgmodeNew)
        {
            if (dbgmodeNew == DBGMODE.DBGMODE_Design)
            {
                Stop();
            }
            else
            {
                Start();
            }
            return 0;
        }

        public void Start()
        {
            if (!started)
            {
                ExceptionTrigger.Instance.notified = false;
                ExceptionTrigger.Instance.DebuggerStarted += Callback_DebuggerStarted;
                debugger.AdviseDebugEventCallback(ExceptionTrigger.Instance);
                started = true;
            }
        }

        private void Callback_DebuggerStarted(uint process_id)
        {
            OnDebuggerStarted(process_id);
        }

        public void Stop()
        {
            if (started)
            {
                OnDebuggerEnded();
                debugger.UnadviseDebugEventCallback(ExceptionTrigger.Instance);
                started = false;
            }
        }
    }
}
