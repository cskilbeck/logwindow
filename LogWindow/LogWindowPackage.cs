﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Microsoft.Internal.VisualStudio.Shell;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;

// TODO Help for using it
// TODO better way of sending log events than a pipe?
// TODO ?? option to output to file when not being debugged?

// NOPE varargs in C# (callerlinenumber/callerfile getting in the way) [NOPE because string interpolation is awesome]
// DONE delete button should be a little 'x'
// DONE hide delete button except when hovering
// DONE fix mouse hover formatting in channels window
// DONE auto-size last column of listview
// DONE detect solution with no channel definitions in it and create 4 defaults
// DONE settings in the solution?
// DONE hover mouse selected listviewitem background color thing (kinda, no hover now)
// DONE C++ log writer - can't get exe name (debugger events!)
// DONE Copy to csv - escape newlines
// DONE Default colors for default filters
// DONE Timestamp format option
// DONE (maybe) send less data over the pipe
// DONE WPF formatting (done for now)
// FIXED scrollbar doesn't update position dynamically
// DONE never delete the pipe! (kinda)
// DONE stick scroll to the botton when user scrolls to the botton
// DONE make it optional to clear the log when debug starts
// DONE nice logging helper class/functions
// DONE double click to line/file (almost, how to activate the editor)
// DONE save config in Project and User files
// DONE settings page in the proper place

namespace LoggingWindow
{
    [Guid(PackageGuidString)]
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [ProvideToolWindow(typeof(LogWindow), Style = VsDockStyle.Tabbed, Orientation = ToolWindowOrientation.Left, Window = EnvDTE.Constants.vsWindowKindOutput)]
    [ProvideToolWindow(typeof(LogChannels), Style = VsDockStyle.Tabbed, Orientation = ToolWindowOrientation.Left, Window = EnvDTE.Constants.vsWindowKindOutput)]
    [ProvideOptionPage(typeof(Options), "Log", "General", 0, 0, true)]
    [ProvideProfile(typeof(Options), "Log", "Settings", 106, 107, isToolsOptionPage:true, DescriptionResourceID = 108)]
    [ProvideToolWindowVisibility(typeof(LogWindow), UIContextGuids.SolutionExists)]
    [ProvideAutoLoad(UIContextGuids.SolutionExists)]
    [ProvideAutoLoad(UIContextGuids.NoSolution)]
    [ProvideAutoLoad(UIContextGuids.EmptySolution)]
    [ProvideAutoLoad(UIContextGuids.CodeWindow)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class LogWindowPackage : Package, IVsPersistSolutionProps, IVsSolutionEvents
    {
        public static System.Windows.Media.Color default_channel_color = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);

        private const string PackageGuidString = "33b6daa9-a7e5-48a8-85e0-c2382eba2095";
        private const string log_config_name = "LogWindowConfig2";
        private const string _strSolutionPersistanceKey = "LogWindowSolutionProperties_v2";
        private const string pipe_guid = "8834E49A-9D77-4166-AD66-EF29CBDA2B13";

        private static Options options;
        private static LogWindowPackage instance;

        private DebuggerEvents mySink;
        private NamedPipeServerStream pipe;
        private Thread pipe_thread;
        private string pipe_name;
        private MemoryStream stream = new MemoryStream();
        private byte[] buffer = new byte[2048];
        private LogWindow log_window;
        private LogChannels log_settings;
        private bool channels_loaded = false;
        private uint solution_events_cookie;

        public LogWindowPackage()
        {
            FileLog.Write("LogWindowPackage()");
            instance = this;
            VSColorTheme_ThemeChanged(null);
            VSColorTheme.ThemeChanged += VSColorTheme_ThemeChanged;
            DumpThemeColors();
        }

        protected override void Initialize()
        {
            FileLog.Write("Initialize()");
            LogWindowCommand.Initialize(this);
            base.Initialize();
            LogChannelsCommand.Initialize(this);

            options = (Options)GetDialogPage(typeof(Options));
            options.PropertyChanged += Options_PropertyChanged;

            IVsSolution solution = GetService(typeof(SVsSolution)) as IVsSolution;
            if (solution != null && solution.AdviseSolutionEvents(this, out solution_events_cookie) != VSConstants.S_OK)
            {
                Debug.WriteLine("ERROR!");
            }

            mySink = new DebuggerEvents((IVsDebugger)GetService(typeof(IVsDebugger)));
            mySink.DebuggerStarted += DebuggerStarted;
            mySink.DebuggerEnded += DebuggerEnded;
        }

        private void VSColorTheme_ThemeChanged(ThemeChangedEventArgs e)
        {
            default_channel_color = VSColorTheme.GetThemedColor(EnvironmentColors.ToolWindowTextColorKey).ToMediaColor();
            LogEntry.LogWindowControl?.RefreshLog();
        }

        void CreateToolWindow<T>(ref T already) where T: class
        {
            if (already == null)
            {
                ToolWindowPane window = FindToolWindow(typeof(T), 0, true);
                if (window == null || window.Frame == null)
                {
                    throw new NotSupportedException("Can't create LogWindow!?");
                }
                already = window as T;
                IVsWindowFrame frame = window.Frame as IVsWindowFrame;
                if (frame != null)
                {
                    ErrorHandler.ThrowOnFailure(frame.ShowNoActivate());
                }
            }
        }

        void ShowLogWindow()
        {
            FileLog.Write("ShowLogWindow()");
            CreateToolWindow(ref log_window);
            CreateToolWindow(ref log_settings);
        }

        public static Options Options
        {
            get
            {
                return options; 
            }
        }

        public static void LaunchHelpUrl()
        {
            string help_url = "https://bitbucket.org/cskilbeck/logwindow/wiki/Home";
            IVsWebBrowsingService wbSvc = (IVsWebBrowsingService)instance.GetService(typeof(SVsWebBrowsingService));
            Guid guidNull = Guid.Empty;
            uint dwCreateFlags = (uint)__VSCREATEWEBBROWSER.VSCWB_AutoShow | (uint)__VSCREATEWEBBROWSER.VSCWB_StartCustom;
            wbSvc.CreateWebBrowser(dwCreateFlags, ref guidNull, "Log Window Help", help_url, null, out IVsWebBrowser browser, out IVsWindowFrame frame);
        }

        void DebuggerStarted(uint process_id)
        {
            FileLog.Write("Debug Mode begins");
            if (Options.ShowOnDebug)
            {
                ShowLogWindow();
            }

            if (LogEntry.LogWindowControl != null)
            {
                FileLog.Write("Got LogWindow");
                LogEntry.TimestampFormatString = Options.TimestampFormat;
                if (Options.ClearOnDebug)
                {
                    LogEntry.LogWindowControl.ClearLog();
                }

                LogEntry.LogWindowControl.auto_scroll = true;
                try
                {
                    FileLog.Write($" Process id: {process_id}");
                    StartPipeThread(string.Format("{0:x8}_{1}", process_id, pipe_guid));
                }
                catch (Exception ex)
                {
                    FileLog.Write("Exception: " + ex.ToString() + ":" + ex.Message);
                }
            }
        }

        void DebuggerEnded()
        {
            FileLog.Write("Debug Mode Break/Design");
            if (Options.ShowOnDebugEnd)
            {
                ShowLogWindow();
            }
        }

        private void Options_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "TimestampFormat":
                    LogEntry.TimestampFormatString = options.TimestampFormat;
                    LogEntry.LogWindowControl?.RefreshLog();
                    break;

                case "Font":
                    LogEntry.LogWindowControl?.SetupFont();
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            FileLog.Write("Dispose()");
            IVsSolution solution = GetService(typeof(SVsSolution)) as IVsSolution;
            solution?.UnadviseSolutionEvents(solution_events_cookie);
            base.Dispose(disposing);
            if (disposing)
            {
                mySink.Dispose();
            }
        }

        void MessageReceiver()
        {
            FileLog.Write("MessageReceiver()");
            while (true)
            {
                IntPtr sender_buffer= IntPtr.Zero;
                try
                {
                    FileLog.Write("pipe.WaitForConnection()");
                    pipe.WaitForConnection();
                    LogEntrySender sender = new LogEntrySender();
                    int sender_size = Marshal.SizeOf(sender);
                    byte[] sender_bytes = new byte[sender_size];
                    sender_buffer = Marshal.AllocHGlobal(sender_size);
                    byte[] channel = new byte[8192];
                    byte[] text = new byte[8192];
                    byte[] filename = new byte[8192];
                    while (pipe.IsConnected)
                    {
                        try
                        {
                            int got = pipe.Read(sender_bytes, 0, sender_size);
                            if (got > 0)
                            {
                                Marshal.Copy(sender_bytes, 0, sender_buffer, sender_size);
                                Marshal.PtrToStructure(sender_buffer, sender);
                                int channel_got = pipe.Read(channel, 0, sender.channel_length); // TODO (chs): lots of error checking here...
                                int text_got = pipe.Read(text, 0, sender.text_length);
                                int filename_got = pipe.Read(filename, 0, sender.filename_length);
                                LogEntry.LogWindowControl.AddLogEntry(new LogEntry()
                                {
                                    TimeStamp = DateTime.FromFileTime(sender.timestamp),
                                    LineNumber = sender.line_number,
                                    Channel = new string(Encoding.UTF8.GetChars(channel, 0, channel_got)),
                                    Text = new string(Encoding.UTF8.GetChars(text, 0, text_got)),
                                    Filename = new string(Encoding.UTF8.GetChars(filename, 0, filename_got))
                                });
                            }
                        }
                        catch (EndOfStreamException)
                        {
                            break;
                        }
                    }
                    pipe.Disconnect();
                }
                catch (IOException e)
                {
                    Debug.WriteLine(e.Message);
                    FileLog.Write("IOException: " + e.Message);
                    break;
                }
                catch (ThreadAbortException e)
                {
                    Debug.WriteLine(e.Message);
                    FileLog.Write("ThreadAbortException: " + e.Message);
                    break;
                }
                finally
                {
                    if(sender_buffer != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(sender_buffer);
                    }
                }
            }
            lock (pipe_lock)
            {
                FileLog.Write("Close Pipe");
                pipe.Close();
                pipe.Dispose();
                pipe = null;
            }
        }

        static object pipe_lock = new object();

        void StartPipeThread(string name)
        {
            FileLog.Write("StartPipeThread: " + name);
            lock (pipe_lock)
            {
                if (pipe == null || pipe_name != name)
                {
                    FileLog.Write("new NamedPipeServerStream");
                    pipe_name = name;
                    pipe = new NamedPipeServerStream(name, PipeDirection.In, 2, PipeTransmissionMode.Byte, PipeOptions.WriteThrough, 1024, 0);
                    pipe_thread = new Thread(MessageReceiver);
                    pipe_thread.Start();
                }
            }
        }

        public void GotoLogEntry(LogEntry e)
        {
            if(e == null)
            {
                return;
            }
            try
            {
                VsShellUtilities.OpenDocument(ServiceProvider.GlobalProvider,
                                            e.Filename,
                                            Guid.Empty, 
                                            out IVsUIHierarchy Hierarchy,
                                            out uint ItemID,
                                            out IVsWindowFrame WindowFrame,
                                            out IVsTextView TextView);
                if (TextView != null)
                {
                    TextSpan span = new TextSpan() { iStartLine = e.LineNumber - 1, iEndLine = e.LineNumber - 1, iStartIndex = 0, iEndIndex = 1 };
                    ErrorHandler.ThrowOnFailure(TextView.EnsureSpanVisible(span));
                    ErrorHandler.ThrowOnFailure(TextView.SetCaretPos(e.LineNumber - 1, 0));
                    ErrorHandler.ThrowOnFailure(TextView.SendExplicitFocus());
                    WindowFrame.Show();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public int SaveUserOptions([In] IVsSolutionPersistence pPersistence)
        {
            pPersistence.SavePackageUserOpts(this, _strSolutionPersistanceKey);
            return VSConstants.S_OK;
        }

        public int WriteUserOptions(IStream pOptionsStream, string pszKey)
        {
            DataStreamFromComStream pStream = new DataStreamFromComStream(pOptionsStream);
            BinaryWriter writer = new BinaryWriter(pStream);
            writer.Write(LogWindowControl.log_channels.Count);
            foreach(Channel c in LogWindowControl.log_channels)
            {
                c.Write(writer);
            }
            return VSConstants.S_OK;
        }

        public int LoadUserOptions(IVsSolutionPersistence pPersistence, uint grfLoadOpts)
        {
            pPersistence.LoadPackageUserOpts(this, _strSolutionPersistanceKey);
            return VSConstants.S_OK;
        }

        public int ReadUserOptions(IStream pOptionsStream, string pszKey)
        {
            DataStreamFromComStream pStream = new DataStreamFromComStream(pOptionsStream);
            BinaryReader reader = new BinaryReader(pStream);
            int channel_count = reader.ReadInt32();
            LogWindowControl.log_channels.Clear();
            for(int i=0; i<channel_count; ++i)
            {
                Channel c = new Channel();
                c.Read(reader);
                LogWindowControl.log_channels.Add(c);
            }
            channels_loaded = true;
            return VSConstants.S_OK;
        }

        public int QuerySaveSolutionProps(IVsHierarchy pHierarchy, VSQUERYSAVESLNPROPS[] pqsspSave)
        {
            return VSConstants.S_OK;
        }

        public int SaveSolutionProps(IVsHierarchy pHierarchy, IVsSolutionPersistence pPersistence)
        {
            return VSConstants.S_OK;
        }

        public int WriteSolutionProps(IVsHierarchy pHierarchy, string pszKey, IPropertyBag pPropBag)
        {
            return VSConstants.S_OK;
        }

        public int ReadSolutionProps(IVsHierarchy pHierarchy, string pszProjectName, string pszProjectMk, string pszKey, int fPreLoad, IPropertyBag pPropBag)
        {
            return VSConstants.S_OK;
        }

        public int OnProjectLoadFailure(IVsHierarchy pStubHierarchy, string pszProjectName, string pszProjectMk, string pszKey)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            if (!channels_loaded)
            {
                LogWindowControl.log_channels.Clear();
                LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(0, 200, 0), Enabled = true, Text = "Debug" });
                LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(0, 200, 200), Enabled = true, Text = "Info" });
                LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(200, 100, 0), Enabled = true, Text = "Warning" });
                LogWindowControl.log_channels.Add(new Channel() { Color = System.Windows.Media.Color.FromRgb(200, 0, 0), Enabled = true, Text = "Error" });
            }
            return VSConstants.S_OK;
        }

        public int OnQueryCloseSolution(object pUnkReserved, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeCloseSolution(object pUnkReserved)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterCloseSolution(object pUnkReserved)
        {
            channels_loaded = false;
            return VSConstants.S_OK;
        }

        private void DumpThemeColors()
        {
#if DEBUG
            //Debug.WriteLine("<html><table width=\"100%\">");
            //foreach (var prop in typeof(Microsoft.VisualStudio.PlatformUI.EnvironmentColors).GetProperties())
            //{
            //    try
            //    {
            //        ThemeResourceKey r = (ThemeResourceKey)prop.GetValue(null);
            //        if (r.KeyType == ThemeResourceKeyType.BackgroundColor)
            //        {
            //            Debug.Write("<tr>");
            //            Debug.Write("<td width=\"20%\">" + prop.Name + "</td>");
            //            System.Drawing.Color c = Microsoft.VisualStudio.PlatformUI.VSColorTheme.GetThemedColor(r);
            //            Debug.Write("<td width=\"80%\" bgcolor=\"" + ColorToHex(c) + "\">&nbsp;</td>");
            //            Debug.WriteLine("</tr>");
            //        }
            //    }
            //    catch (InvalidCastException)
            //    {
            //    }
            //}
            //Debug.WriteLine("</table></html>");
#endif
        }
    }
}
