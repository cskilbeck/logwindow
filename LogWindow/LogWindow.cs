﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;

namespace LoggingWindow
{
    [Guid("40b360e8-fa34-4993-96fb-b1574b2919b5")]
    public class LogWindow : ToolWindowPane
    {
        public LogWindow() : base(null)
        {
            this.Caption = "Log";
            this.Content = new LogWindowControl();
        }
    }
}
