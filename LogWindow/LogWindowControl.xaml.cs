﻿        using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Media;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Automation.Peers;
using WinForms = System.Windows.Forms;
using System.IO;

namespace LoggingWindow
{
    public partial class LogWindowControl : UserControl
    {
        public static ObservableCollection<LogEntry> log_items = new ObservableCollection<LogEntry>();
        public static ObservableCollection<Channel> log_channels = new ObservableCollection<Channel>();

        private ScrollViewer scroll_viewer;

        public int IndexOf(string channel)
        {
            for (int i = 0; i < log_channels.Count; ++i)
            {
                if (channel == log_channels[i].Text)
                {
                    return i;
                }
            }
            return -1;
        }

        public void AddLogEntry(string channel, string text)
        {
            AddLogEntry(new LogEntry { Channel = channel, Text = text, TimeStamp = DateTime.Now });
        }

        private bool _auto_scroll = true;

        public bool auto_scroll
        {
            get
            {
                return _auto_scroll;
            }
            set
            {
                _auto_scroll = value;
            }
        }

        public void AddLogEntry(LogEntry e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                log_items.Add(e);
                if(auto_scroll && scroll_viewer != null)
                {
                    isUserScroll = false;
                    scroll_viewer.ScrollToBottom();
                }
            }));
        }

        public LogWindowControl()
        {
            LogEntry.LogWindowControl = this;

            this.InitializeComponent();

            SetupFont();

            var view = (CollectionView)CollectionViewSource.GetDefaultView(log_items);
            view.Filter = FilterLogItems;
            Log.ItemsSource = view;

            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                LogChannels twp = LogChannelsCommand.LogChannels;
                if (twp != null)
                {
                    twp.LogWindowControl = this;
                    var ctrl = (LogChannelsControl)twp.Content;
                    ctrl.Config.ItemsSource = log_channels;
                }
            }));
        }

        public LogChannelsControl GetLogChannelsControl
        {
            get
            {
                return (LogChannelsControl)(LogChannelsCommand.LogChannels?.Content);
            }
        }

        public void RefreshLog()
        {
            CollectionViewSource.GetDefaultView(log_channels).Refresh();
            CollectionViewSource.GetDefaultView(log_items).Refresh();
        }

        public int MatchChannel(string channel)
        {
            for (int i = 0; i < log_channels.Count; ++i)
            {
                if (log_channels[i].Match(channel))
                {
                    return i;
                }
            }
            return -1;
        }

        public Color GetColor(string channel)
        {
            int index = MatchChannel(channel);  // TODO (chs): PERF: MatchChannel being called for Color and Visibility... (cached channel_id ?)
            if (index != -1)
            {
                return log_channels[index].Color;
            }
            return LogWindowPackage.default_channel_color;
        }

        public bool GetVisible(string channel)
        {
            int index = MatchChannel(channel);
            if (index != -1)
            {
                return log_channels[index].Enabled;
            }
            return true;
        }

        public void ChangeColor(string channel, Color c)
        {
            int i = IndexOf(channel);
            if (i != -1)
            {
                log_channels[i].Color = c;
                RefreshLog();
            }
        }

        public bool FilterLogItems(object item)
        {
            return GetVisible((item as LogEntry).Channel);
        }

        private void MenuItem_Click_clear(object sender, RoutedEventArgs e)
        {
            ClearLog();
        }

        public void ClearLog()
        {
            log_items.Clear();
        }

        public void SetupFont()
        {
            var options = LogWindowPackage.Options;
            if(options != null)
            {
                System.Drawing.Font f = options.CurrentFont;
                FontFamily family = (FontFamily)new FontFamilyConverter().ConvertFromString(f.Name);
                FontStyle style = FontStyles.Normal;
                FontWeight weight = FontWeights.Regular;
                if (f.Italic)
                {
                    style = FontStyles.Italic;
                }
                if (f.Bold)
                {
                    weight = FontWeights.Bold;
                }
                Log.FontStyle = style;
                Log.FontWeight = weight;
                Log.FontFamily = family;
                Log.FontSize = f.SizeInPoints / 72 * 96;
            }
        }

        private void Export(IEnumerable<object> items, string filename)
        {
            if(filename == null)
            {
                return;
            }
            // Export it
            try
            {
                using (StreamWriter s = new StreamWriter(filename))
                {
                    s.WriteLine("Date,Time,Channel,Text");
                    foreach (var l in items)
                    {
                        s.WriteLine(l.ToString());
                    }
                }
            }
            catch (IOException ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private IEnumerable<object> SelectedItems()
        {
            foreach (var item in Log.SelectedItems)
            {
                yield return item;
            }
        }

        private IEnumerable<object> VisibleItems()
        {
            foreach (var item in Log.Items)
            {
                yield return item;
            }
        }

        private IEnumerable<object> AllItems()
        {
            foreach (var item in log_items)
            {
                yield return item;
            }
        }

        private string GetExportFilename(string title)
        {
            // AARGH - PLEASE CAN WE HAVE LOCAL 'using'?
            WinForms.SaveFileDialog save_dialog = new WinForms.SaveFileDialog()
            {
                Title = title,
                AddExtension = true,
                CheckPathExists = true,
                DefaultExt = "csv",
                Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*",
                OverwritePrompt = true,
                SupportMultiDottedExtensions = true
            };
            if (save_dialog.ShowDialog() == WinForms.DialogResult.OK)
            {
                return save_dialog.FileName;
            }
            return null;
        }

        private void MenuItem_Click_export_selected(object sender, RoutedEventArgs e)
        {
            Export(SelectedItems(), GetExportFilename("Export all log messages"));
        }

        private void MenuItem_Click_export_visible(object sender, RoutedEventArgs e)
        {
            Export(VisibleItems(), GetExportFilename("Export visible log messages"));
        }

        private void MenuItem_Click_export_all(object sender, RoutedEventArgs e)
        {
            Export(AllItems(), GetExportFilename("Export all log messages"));
        }

        private void MenuItem_Click_copy(object sender, RoutedEventArgs e)
        {
            StringBuilder b = new StringBuilder();
            foreach (var l in Log.SelectedItems)
            {
                b.Append(l.ToString());
                b.Append("\n");
            }
            Clipboard.SetText(b.ToString());
        }

        private void MenuItem_Click_config(object sender, RoutedEventArgs e)
        {
            LogChannelsCommand.Instance.ShowToolWindow(null, null);
        }

        private void MenuItem_Click_options(object sender, RoutedEventArgs e)
        {
            LogWindowCommand.Instance.LogWindowPackage.ShowOptionPage(typeof(Options));
        }

        private void MenuItem_Click_filter(object sender, RoutedEventArgs e)
        {
            if (Log.SelectedIndex != -1)
            {
                SetupFilter(Log.SelectedItems[0] as LogEntry);
            }
        }

        public void SetupFilter(LogEntry log_entry)
        {
            System.Windows.Forms.ColorDialog d = new System.Windows.Forms.ColorDialog()
            {
                AllowFullOpen = true,
                AnyColor = true,
                FullOpen = true,
                SolidColorOnly = true,
                Color = LogWindowPackage.default_channel_color.ToDrawingColor()
            };
            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var c = d.Color;
                log_channels.Insert(0, new Channel { Text = log_entry.Channel, Enabled = true, Color = Color.FromRgb(c.R, c.G, c.B) });
                RefreshLog();
            }
        }

        private void Log_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LogWindowCommand.Instance.LogWindowPackage.GotoLogEntry(Log.SelectedItem as LogEntry);
        }

        private volatile bool isUserScroll = true;

        private void Log_ScrollChanged(object sender, RoutedEventArgs e)
        {
            var e2 = e as ScrollChangedEventArgs;
            scroll_viewer = e.OriginalSource as ScrollViewer;
            if (e2.VerticalChange != 0)
            {
                if (isUserScroll)
                {
                    if(e2.VerticalChange < 0)
                    {
                        auto_scroll = false;
                    }
                    if (scroll_viewer.VerticalOffset - scroll_viewer.ScrollableHeight > -1 || scroll_viewer.ScrollableHeight < scroll_viewer.ExtentHeight)
                    {
                        auto_scroll = true;
                    }
                }
                isUserScroll = true;
            }
        }

        private void MenuItem_Click_help(object sender, RoutedEventArgs e)
        {
            LogWindowPackage.LaunchHelpUrl();
        }
    }
}
