﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Media;

namespace LoggingWindow
{
    public class LogEntry
    {
        public static LogWindowControl LogWindowControl;

        public DateTime TimeStamp { get; set; }
        public string Channel { get; set; }
        public string Text { get; set; }
        public string Filename { get; set; }
        public int LineNumber { get; set; }

        private static string timestamp_format_string = "g";
        private static string timestamp_export_format_string = "yyyy-MM-dd,hh:mm:ss.fff";

        public static string TimestampFormatString
        {
            set
            {
                timestamp_format_string = value;
            }
        }

        public Brush Color
        {
            get
            {

                return new SolidColorBrush(LogWindowControl.GetColor(Channel));
            }
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2}", TimeStamp.ToString(timestamp_export_format_string), Escape(Channel), Escape(Text));
        }

        public string FormattedTimestamp
        {
            get
            {
                return TimeStamp.ToString(timestamp_format_string);
            }
        }

        static string Escape(string s)
        {
            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            return s;
        }

        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };
    }

    [StructLayout(LayoutKind.Sequential)]
    public class LogEntrySender
    {
        [MarshalAs(UnmanagedType.U8)]
        public long timestamp;
        [MarshalAs(UnmanagedType.U4)]
        public int line_number;
        [MarshalAs(UnmanagedType.U4)]
        public int channel_length;
        [MarshalAs(UnmanagedType.U4)]
        public int text_length;
        [MarshalAs(UnmanagedType.U4)]
        public int filename_length;
    }
}
