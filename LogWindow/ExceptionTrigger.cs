﻿using System;
using System.Net;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Debugger;
using Microsoft.VisualStudio.Debugger.ComponentInterfaces;
using Microsoft.VisualStudio.Debugger.Exceptions;
using Microsoft.VisualStudio.Debugger.Interop;

namespace LoggingWindow
{
    public class ExceptionTrigger : IDebugEventCallback2, IDkmExceptionTriggerHitNotification
    {
        private static readonly Lazy<ExceptionTrigger> instance = new Lazy<ExceptionTrigger>();

        public delegate void DebuggerStartedDelegate(uint process_id);
        public event DebuggerStartedDelegate DebuggerStarted;

        public bool notified = false;

        private static Guid Exception_sourceId = new Guid("fd41eacb-8199-445f-9179-a82b46e46f77");

        public static ExceptionTrigger Instance
        {
            get
            {
                if (!instance.IsValueCreated)
                {
                    FileLog.Write("Instance creation imminent!");
                }
                return instance.Value;
            }
        }

        public int Event(IDebugEngine2 pEngine, IDebugProcess2 pProcess, IDebugProgram2 pProgram, IDebugThread2 pThread, IDebugEvent2 pEvent, ref Guid riidEvent, uint dwAttrib)
        {
            if (riidEvent == typeof(IDebugProgramCreateEvent2).GUID)
            {
                try
                {
                    FileLog.Write("GUID: " + riidEvent.ToString());

                    AD_PROCESS_ID[] process_id = new AD_PROCESS_ID[1];
                    if (!notified && pProcess != null && pProcess.GetPhysicalProcessId(process_id) == VSConstants.S_OK)
                    {
                        FileLog.Write("Process_ID: " + process_id[0].dwProcessId);

                        if (pProcess.GetProcessId(out Guid processId) == VSConstants.S_OK)
                        {
                            FileLog.Write("GUID_Process_ID: " + processId);

                            DkmProcess dkmProcess = DkmProcess.FindProcess(processId);
                            if (dkmProcess != null)
                            {
                                FileLog.Write("Found dkmProcess");
                                var debugTrigger = DkmExceptionCodeTrigger.Create(DkmExceptionProcessingStage.Thrown, null, DkmExceptionCategory.Win32, 0xFF123456);
                                dkmProcess.AddExceptionTrigger(Exception_sourceId, debugTrigger);
                                FileLog.Write("AddExceptionTrigger complete");
                                DebuggerStarted?.Invoke(process_id[0].dwProcessId);
                                notified = true;
                            }
                        }
                    }
                }
                finally
                {
                    if (pEngine != null && Marshal.IsComObject(pEngine)) Marshal.ReleaseComObject(pEngine);
                    if (pProcess != null && Marshal.IsComObject(pProcess)) Marshal.ReleaseComObject(pProcess);
                    if (pProgram != null && Marshal.IsComObject(pProgram)) Marshal.ReleaseComObject(pProgram);
                    if (pThread != null && Marshal.IsComObject(pThread)) Marshal.ReleaseComObject(pThread);
                    if (pEvent != null && Marshal.IsComObject(pEvent)) Marshal.ReleaseComObject(pEvent);
                }
            }
            return 0;
        }

        void IDkmExceptionTriggerHitNotification.OnExceptionTriggerHit(DkmExceptionTriggerHit hit, DkmEventDescriptorS eventDescriptor)
        {
            FileLog.Write("OnExceptionTriggerHit");
            var exceptionInfo = hit.Exception as Microsoft.VisualStudio.Debugger.Native.DkmWin32ExceptionInformation;
            if (exceptionInfo.Code == 0xFF123456)
            {
                const int exceptionParameterCount = 1;  // IP of Xbox (incoming), IP of debug machine (outgoing)
                if (exceptionInfo.ExceptionParameters.Count == exceptionParameterCount)
                {
                    byte[] buffer = new byte[4];

                    IPAddress ip_address = new IPAddress(0x08080808);// Extensions.GetMyIPAddress();

                    // read the IP of the target (might be the same as the debugger)
                    hit.Process.ReadMemory(exceptionInfo.ExceptionParameters[0], DkmReadMemoryFlags.None, buffer);

                    // Write back IP of debugger
                    hit.Process.WriteMemory(exceptionInfo.ExceptionParameters[0], BitConverter.GetBytes(true));
                }
            }
            eventDescriptor.Suppress();
        }
    }
}
