﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Media;

namespace LoggingWindow
{
    public class Channel
    {
        public Color Color { get; set; }

        public bool Enabled { get; set; }

        public string Text
        {
            get
            {
                return regex.ToString();
            }
            set
            {
                try
                {
                    regex = new Regex(value);
                }
                catch (Exception)
                {
                }
            }
        }

        private Regex regex;

        public bool Match(string t)
        {
            return regex.Match(t).Success;
        }

        public string ColorString => string.Format("#FF{0:x2}{1:x2}{2:x2}", Color.R, Color.G, Color.B);

        public static Color ColorFromString(string s)
        {
            try
            {
                string a = s.Substring(1, 2);
                string r = s.Substring(3, 2);
                string g = s.Substring(5, 2);
                string b = s.Substring(7, 2);
                byte A = byte.Parse(a, System.Globalization.NumberStyles.HexNumber);
                byte R = byte.Parse(r, System.Globalization.NumberStyles.HexNumber);
                byte G = byte.Parse(g, System.Globalization.NumberStyles.HexNumber);
                byte B = byte.Parse(b, System.Globalization.NumberStyles.HexNumber);
                return Color.FromArgb(A, R, G, B);
            }
            catch (IndexOutOfRangeException)
            {
            }
            catch (FormatException)
            {
            }
            return Color.FromRgb(128, 128, 128);
        }

        public void Read(BinaryReader r)
        {
            Text = r.ReadString();
            Enabled = r.ReadBoolean();
            Color = ColorFromString(r.ReadString());
        }

        public void Write(BinaryWriter w)
        {
            w.Write(Text);
            w.Write(Enabled);
            w.Write(ColorString);
        }
    }
}
